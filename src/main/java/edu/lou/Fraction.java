package edu.lou;

import lombok.Value;

@Value
public class Fraction {
	private final int numerator;
	private final int denominator;
}
